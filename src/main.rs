mod widgets;
use widgets::{battery, date, disk, Widget};

use std::io::{stdin, BufRead, BufReader, ErrorKind, Write};
use std::process;
use std::sync::mpsc;
use std::thread;
use std::time;

enum Status {
    Battery(String),
    Date(String),
    Disk(String),
    Bspc(String),
}

#[derive(Default)]
struct Out {
    battery: String,
    date: String,
    disk: String,
    bspc: String,
}

fn main() {
    let (send, receive) = mpsc::channel();

    let battery = send.clone();
    thread::spawn(move || loop {
        battery
            .send(Status::Battery(battery::new("BAT0").report().unwrap()))
            .unwrap();
        thread::sleep(time::Duration::from_secs(60));
    });

    let date = send.clone();
    thread::spawn(move || loop {
        date.send(Status::Date(date::new("%d.%m.%Y %H:%M").report().unwrap()))
            .unwrap();
        thread::sleep(time::Duration::from_secs(60));
    });

    let disk = send.clone();
    thread::spawn(move || loop {
        disk.send(Status::Disk(disk::new("/").report().unwrap()))
            .unwrap();
        thread::sleep(time::Duration::from_secs(60));
    });

    // stdin reader
    let bspc = send.clone();
    thread::spawn(move || {
        let mut buffer = String::new();
        let mut reader = BufReader::new(stdin());

        loop {
            // read_line return number of bytes read
            let data = reader.read_line(&mut buffer).unwrap();
            if data != 0 {
                bspc.send(Status::Bspc(parse_bspc_report(&buffer))).unwrap();
                buffer.clear();
            }
        }
    });

    let mut out = Out {
        ..Default::default()
    };

    let mut buffer = String::new();

    loop {
        let mut bar = process::Command::new("lemonbar")
            .arg("-f")
            .arg("*-terminus-*")
            .stdin(process::Stdio::piped())
            .spawn()
            .expect("unable to run lemonbar");

        let stdin = bar.stdin.as_mut().unwrap();

        if buffer.len() > 0 && send_to_child(stdin, buffer.as_bytes()).is_none() {
            bar.wait().unwrap();
            eprintln!("restarting lemonbar");
            break;
        }

        for msg in receive.iter() {
            // if we're here that means existing buffer, if there was one,
            // was already used, clear it!
            buffer.clear();

            match msg {
                Status::Battery(item) => out.battery = item,
                Status::Date(item) => out.date = item,
                Status::Disk(item) => out.disk = item,
                Status::Bspc(item) => out.bspc = item,
            }

            // save message
            buffer = format!(
                "%{{l}}{}%{{r}}{} | {} | {}\n",
                out.bspc, out.battery, out.disk, out.date
            );

            if send_to_child(stdin, buffer.as_bytes()).is_none() {
                bar.wait().unwrap();
                eprintln!("restarting lemonbar");
                break;
            }
        }
    }
}

fn send_to_child(stdin: &mut std::process::ChildStdin, message: &[u8]) -> Option<()> {
    match stdin.write_all(message) {
        Err(e) => {
            match e.kind() {
                // if child dies, pipe is broken and rust reports:
                // thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: Os { code: 32, kind: BrokenPipe, message: "Broken pipe" }'
                ErrorKind::BrokenPipe => {
                    return None;
                }
                _ => {
                    println!("unknown error: {:?}", e);
                    return None;
                }
            };
        }

        _ => {
            return Some(());
        }
    }
}

fn parse_bspc_report(input: &str) -> String {
    let separator = ":";

    let mut output: Vec<String> = Vec::new();

    for item in input.split(separator) {
        let head = item.chars().take(1).collect::<String>();
        let tail = item.chars().skip(1).collect::<String>();

        match head.as_str() {
            "O" | "F" | "U" => output.push(format!("%{{R}} {} %{{R}}", tail)),
            "o" | "f" | "u" => {
                output.push(format!(" {} ", tail));
            }
            _ => {
                // eprintln!("unknown item: {}", item);
            }
        }
    }

    output.join("")
}

#[test]
fn test_parse_bspc_report() {
    // message example:
    // WMeDP1:oedit:Oshell:owww:fmisc:LT:TT:G

    // M = focused
    // m = unfocused
    let monitors_input = "Mactive:minactive";

    // O = occupied, focused
    // o = occupied, unfocused
    // F = free, focused
    // f = free, unfocused
    // U = urgent, focused (not sure what urgent is)
    // u = urget, unfocused (not sure what urgent is)
    let desktops_input = "oeditor:Oshell:oweb:fmusic:Uurgentfocused:uurgentunfocused";

    let input = format!("{}:{}", monitors_input, desktops_input);
    assert_eq!(
        parse_bspc_report(&input),
        " editor %{R} shell %{R} web  music %{R} urgentfocused %{R} urgentunfocused "
    );
}
