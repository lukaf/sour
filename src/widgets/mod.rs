pub mod battery;
pub mod date;
pub mod disk;

pub trait Widget {
    fn report(&self) -> Result<String, String>;
}
