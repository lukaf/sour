extern crate chrono;

use super::Widget;
use chrono::prelude::Local;

pub struct Date {
    format: String,
}

pub fn new(fmt: &str) -> Date {
    Date {
        format: fmt.to_string(),
    }
}

impl Widget for Date {
    fn report(&self) -> Result<String, String> {
        Ok(Local::now().format(&self.format).to_string())
    }
}
