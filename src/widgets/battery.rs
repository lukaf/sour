use super::Widget;
use std::fs;

pub struct Battery {
    status: String,
    capacity: String,
}

pub fn new(id: &str) -> Battery {
    Battery {
        capacity: format!("/sys/class/power_supply/{}/capacity", id),
        status: format!("/sys/class/power_supply/{}/status", id),
    }
}

impl Widget for Battery {
    fn report(&self) -> Result<String, String> {
        let capacity = match fs::read_to_string(&self.capacity) {
            Ok(v) => v,
            Err(e) => return Err(e.to_string()),
        };

        let capacity: u32 = match capacity.trim().parse() {
            Ok(v) => v,
            Err(e) => return Err(format!("what? {}", e.to_string())),
        };

        let colour = match capacity {
            0...24 => "#FF0000",
            25...49 => "#FFAE00",
            _ => "#00FF00",
        };

        let status = match fs::read_to_string(&self.status) {
            Ok(v) => v,
            Err(e) => return Err(e.to_string()),
        };

        let icon = match status.trim_end() {
            "Charging" => "▲",
            "Discharging" => "▼",
            "Full" => "■",
            _ => "●",
        };

        Ok(format!("%{{F{}}}{} {}%%%{{F-}}", colour, icon, capacity))
    }
}
