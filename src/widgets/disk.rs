use super::Widget;
use std::process::Command;

pub struct Disk {
    mountpoint: String,
}

pub fn new(mountpoint: &str) -> Disk {
    Disk {
        mountpoint: mountpoint.to_string(),
    }
}

impl Widget for Disk {
    fn report(&self) -> Result<String, String> {
        let output = match Command::new("df").arg("-h").output() {
            Ok(v) => v,
            Err(e) => return Err(e.to_string()),
        };

        let result: String = String::from_utf8(output.stdout)
            .unwrap()
            .lines()
            .filter(|line| line.ends_with(&self.mountpoint))
            .collect();

        let items: Vec<&str> = result.split_whitespace().collect();

        Ok(format!("{}: {}", &self.mountpoint, items[items.len() - 3]))
    }
}
