use std::fs;

pub fn read_file(filename: &str) -> Result<String, String> {
    match fs::read_to_string(filename) {
        Ok(data) => data,
        Err(e) => Err(e.to_string()),
    }
}
